# READ ME

## Problema de registro de empleados

Para resolver el problema del registro de employees, se creó una funcion en EmployeesTable para obtener el ID del último empleado y que ese fuera el valor del campo EMP_NO, ya que el problema era que siempre quería agregar el registro con el emp_no = 0, pues el campo no es auto incremental.

## Empleados del departamento Finance con un salario mayor a 100,000

Para resolver este problema, se utilizó la función `join()`, en vez de la función `contain()` vista en los tutoriales del curso. Ya que se tenía el problema en el where a la hora de hacer el `contain()` desde Employees a una de sus tablas hijas.

## Layout de inicio

Para el layout de inicio, se utilizó la funcion 

```bash
<?= $this->Html->link(__('Empleados'), ['controller' => 'employees', 'action' => 'index']) ?>
```
Para crear el menú y se implementó el framework [Bootstrap 4](https://getbootstrap.com/)

## Login y Logout

Para el Login y Logout se utilizó el componente Auth, tal y como se vio en el tutorial del curso. Se agregó un botón de cierre de sesión en el layout `default.ctp` para que se pudiera cerrar sesión en cualquier controlador.

## Cambio de contraseña

Para el cambio de contraseña, se creó la función cambiarContrasena() en el controlador `EmployeesController`, la cual obtenía el registro del usuario actual mediante la función user() del componente Auth.

## Estilos

Se hicieron algunos cambios en el archivo style.css para modificar el estilo del sistema.

## Routes

Se hizo la traducción de todas las rutas al español.

## CakePHP Application Skeleton

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

If Composer is installed globally, run

```bash
composer create-project --prefer-dist cakephp/app
```

In case you want to use a custom app dir name (e.g. `/myapp/`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Since this skeleton is a starting point for your application and various files
would have been modified as per your needs, there isn't a way to provide
automated upgrades, so you have to do any updates manually.

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.

## Layout

The app skeleton uses a subset of [Foundation](http://foundation.zurb.com/) (v5) CSS
framework by default. You can, however, replace it with any other library or
custom styles.
