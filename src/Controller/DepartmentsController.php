<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Departments Controller
 *
 * @property \App\Model\Table\DepartmentsTable $Departments
 *
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepartmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        //Obtiene los datos del departamento seleccionado a través de su id
        $departments = $this->paginate($this->Departments);

        $this->set(compact('departments'));
    }

    /**
     * View method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //Obtiene a los empleados a través de su id
        $department = $this->Departments->get($id, [
            'contain' => [],
        ]);

        //Envia la información del empleado a la vista
        $this->set('department', $department);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Crea una entidad del tipo departamento para agregar los nuevos datos
        $department = $this->Departments->newEntity();
        //En caso de que se envíe la solicitud para registrar un empleado, ejecuta las instrucciones
        if ($this->request->is('post')) {
             //Realiza la solicitud para guardar los datos
            $department = $this->Departments->patchEntity($department, $this->request->getData());
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->Departments->save($department)) {
                //En caso de que se haya guardado los datos del empleado con éxito, muestra un mensaje
                $this->Flash->success(__('El departamento se ha guardado con éxito.'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo empleado, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el departamento. Por favor, intente nuevamente.'));
        }
        //Envia la información del empleado a la vista
        $this->set(compact('department'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Obtiene el registro a editar a través de su id
        $department = $this->Departments->get($id, ['contain' => [],]);
        //Se solicita el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad para guardar los nuevos datos de la entidad a editar. Esto para poder editar la llave primaria*/
            $newDepartment = $this->Departments->newEntity();
            //Elimina el registro que se editó para que no se duplique
            $this->Departments->delete($department);
            //Se obtiene la información del registro que queremos editar
            $newDepartment = $this->Departments->patchEntity($newDepartment, $this->request->getData());
            //Valida si se guardó de forma correcta el registro
            if ($this->Departments->save($newDepartment)) {
                
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Flash->success(__('El departamento se ha guardado con éxito.'));
                //Se redirige a la página del index del mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro, se muestra un mensaje de error
            $this->Flash->error(__('No se puede guardar el departamento. Por favor, intente nuevamente.'));
        }
        //Se manda la información del título al editar a la vista
        $this->set(compact('department'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //En caso de que se envíe la solicitud para eliminar un empleado mediante post o delete, ejecuta las instrucciones
        $this->request->allowMethod(['post', 'delete']);
        //Obtiene los datos del departamento mediante su id
        $department = $this->Departments->get($id);
        //Valida si se pudo hacer la eliminación con éxito
        if ($this->Departments->delete($department)) {
            //En caso de que se haya eliminado con éxito se envía un mensaje de éxito
            $this->Flash->success(__('El departamento se ha eliminado con éxito.'));
        } else {
            //En caso de que no se haya eliminado con éxito, envía un mensaje de error
            $this->Flash->error(__('No se puede eliminar el departamento. Por favor, intente nuevamente.'));
        }
        //Redirige al index del controlador de empleados
        return $this->redirect(['action' => 'index']);
    }
}
