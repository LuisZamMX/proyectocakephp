<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptEmp Controller
 *
 * @property \App\Model\Table\DeptEmpTable $DeptEmp
 *
 * @method \App\Model\Entity\DeptEmp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptEmpController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptEmp = $this->paginate($this->DeptEmp);

        $this->set(compact('deptEmp'));
    }

    /**
     * View method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function view($id = null, $dept_no = null)
    {   
        // Se agregó el depto_no como parámetro pues es parte de la llave primaria
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        // Se envían los datos del registro a la vista
        $this->set('deptEmp', $deptEmp);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Crea una entidad del tipo empleado para agregar los nuevos datos
        $deptEmp = $this->DeptEmp->newEntity();
        //En caso de que se envíe la solicitud para crear un registro, ejecuta las instrucciones
        if ($this->request->is('post')) {
            //Realiza la solicitud para guardar los datos
            $deptEmp = $this->DeptEmp->patchEntity($deptEmp, $this->request->getData());
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->DeptEmp->save($deptEmp)) {
                //En caso de que se haya guardado los datos del empleado con éxito, muestra un mensaje
                $this->Flash->success(__('El registro se ha guardado con éxito.'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo empleado, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el registro. Por favor, intente nuevamente.'));
        }
        //Envia la información del empleado a la vista
        $this->set(compact('deptEmp'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function edit($id = null, $dept_no = null)
    {
        //Obtiene el registro a través de su id
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        //Se solicita el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
             //Se crea una nueva entidad para guardar los nuevos datos de la entidad a editar. Esto para poder editar la llave primaria
            $newDeptEmp = $this->DeptEmp->newEntity();
            //Se obtiene la información del registro que queremos editar
            $newDeptEmp = $this->DeptEmp->patchEntity($newDeptEmp, $this->request->getData());
            //Valida si se guardó de forma correcta el registro
            if ($this->DeptEmp->save($newDeptEmp)) {
                //En caso de que se haya guardado con éxito, elimina el registro que se editó para que no se duplique
                $this->DeptEmp->delete($deptEmp);
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Flash->success(__('El registro se ha guardado con éxito.'));
                //Se redirige a la página del index del mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro, se muestra un mensaje de error
            $this->Flash->error(__('No se puede guardar el registro. Por favor, intente nuevamente.'));
        }
        //Se manda la información del título al editar a la vista
        $this->set(compact('deptEmp'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dept Emp id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function delete($id = null, $dept_no = null)
    {
        //Solicita el método utilizado para la eliminación
        $this->request->allowMethod(['post', 'delete']);
        //Obtiene los datos del registro mediante su id
        $deptEmp = $this->DeptEmp->get([$id, $dept_no]);
        //Valida si se pudo hacer la eliminación con éxito
        if ($this->DeptEmp->delete($deptEmp)) {
            //En caso de que se haya eliminado con éxito se envía un mensaje de éxito
            $this->Flash->success(__('El registro se ha eliminado con éxito.'));
        } else {
            //En casod e que no se haya eliminado con éxito, envía un mensaje de error
            $this->Flash->error(__('No se puede eliminar el registro. Por favor, intente nuevamente.'));
        }
        //Redirige al index del controlador de DeptEmp
        return $this->redirect(['action' => 'index']);
    }

}
