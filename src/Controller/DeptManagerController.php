<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeptManager Controller
 *
 * @property \App\Model\Table\DeptManagerTable $DeptManager
 *
 * @method \App\Model\Entity\DeptManager[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeptManagerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $deptManager = $this->paginate($this->DeptManager);

        $this->set(compact('deptManager'));
    }

    /**
     * View method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function view($id = null, $dept_no = null)
    {
        // Se agregó el depto_no como parámetro pues es parte de la llave primaria
        $deptManager = $this->DeptManager->get([$id, $dept_no]);
        // Se envían los datos del registro a la vista
        $this->set('deptManager', $deptManager);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Crea una entidad del tipo DeptManager para agregar los nuevos datos
        $deptManager = $this->DeptManager->newEntity();
        //En caso de que se envíe la solicitud para crear un registro, ejecuta las instrucciones
        if ($this->request->is('post')) {
            //Realiza la solicitud para guardar los datos
            $deptManager = $this->DeptManager->patchEntity($deptManager, $this->request->getData());
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->DeptManager->save($deptManager)) {
                //En caso de que se haya guardado los datos del registro con éxito, muestra un mensaje
                $this->Flash->success(__('El registro se ha guardado con éxito.'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo registro, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el registro. Por favor, intente nuevamente.'));
        }
        //Envía los datos del registro a la vista
        $this->set(compact('deptManager'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function edit($id = null, $dept_no)
    {
        //Obtiene el registro a través de su id
        $deptManager = $this->DeptManager->get([$id, $dept_no]);
        //Se solicita el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad para guardar los nuevos datos de la entidad a editar. Esto para poder editar la llave primaria*/
            $newDeptManager = $this->DeptManager->newEntity();
            //Se obtiene la información del registro que queremos editar
            $newDeptManager = $this->DeptManager->patchEntity($newDeptManager, $this->request->getData());
            //Valida si se guardó de forma correcta el registro
            if ($this->DeptManager->save($newDeptManager)) {
                //En caso de que se haya guardado con éxito, elimina el registro que se editó para que no se duplique
                $this->DeptManager->delete($deptManager);
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Flash->success(__('El registro se ha guardado con éxito.'));
                //Se redirige a la página del index del mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro, se muestra un mensaje de error
            $this->Flash->error(__('No se puede guardar el registro. Por favor, intente nuevamente.'));
        }
        //Se manda la información del título al editar a la vista
        $this->set(compact('deptManager'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dept Manager id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el depto_no como parámetro pues es parte de la llave primaria
    public function delete($id = null, $dept_no = null)
    {
        //Solicita el método utilizado para la eliminación
        $this->request->allowMethod(['post', 'delete']);
        //Obtiene los datos del registro mediante su id
        $deptManager = $this->DeptManager->get([$id, $dept_no]);
        //Valida si se pudo hacer la eliminación con éxito
        if ($this->DeptManager->delete($deptManager)) {
            //En caso de que se haya eliminado con éxito se envía un mensaje de éxito
            $this->Flash->success(__('El registro se ha eliminado con éxito.'));
        } else {
            //En caso de que no se haya eliminado con éxito, envía un mensaje de error
            $this->Flash->error(__('No se puede eliminar el registro. Por favor, intente nuevamente.'));
        }
        //Redirige al index del controlador de DeptEmp
        return $this->redirect(['action' => 'index']);
    }
}
