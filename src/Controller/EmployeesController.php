<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
    *Método que inicializa el controlador
    *
    * @return \Cake\Http\Response|null
    */
    public function initialize()
    {
        parent::initialize();
        /*
        Se carga el modelo de salarios y de los departamentos por empleado. para acceder a sus datos en listaEmpleadosFinance()
        */
        $this->loadModel('Salaries');
        $this->loadModel('DeptEmp');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $employees = $this->paginate($this->Employees);
        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {  
        //Obtiene los datos del empleado seleccionado a través de su id
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        //Envia la información del empleado a la vista
        $this->set('employee', $employee);
    }

    /**
     * Agregar un empleado
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {  
        //Crea una entidad del tipo empleado para agregar los nuevos datos
        $employee = $this->Employees->newEntity();
        //En caso de que se envíe la solicitud para registrar un empleado, ejecuta las instrucciones
        if ($this->request->is('post')) {
            //Realiza la solicitud para guardar los datos
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Se obtiene manualmente el ID del último empleado, pues el campo no es AUTO_INCREMENT
            $employee -> emp_no = $this->Employees->nextID()->MaxEmp;
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->Employees->save($employee)) {
                //En caso de que se haya guardado los datos del empleado con éxito, muestra un mensaje
                $this->Flash->success(__('El empleado se ha guardado con éxito'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo empleado, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el empleado. Por favor, intente nuevamente.'));
        }
        //Envia la información del empleado a la vista
        $this->set(compact('employee'));
    }

    /**
     * Editar empleados
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Obtiene al empleados a través de su id
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        //En caso de que se envíe la solicitud para registrar un empleado mediante patch, post o put, ejecuta las instrucciones
        if ($this->request->is(['patch', 'post', 'put'])) {
            //Crea una nueva entidad para traer los datos del empleado que se quiere editar
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Se valida que se haya hecho el guardado con éxito
            if ($this->Employees->save($employee)) {
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Flash->success(__('El empleado se ha guardado con éxito.'));
                //Se redirige al index del controlador de empleados
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que nos e guarden con éxito, se mandar un mensaje de error
            $this->Flash->error(__('No se puede guardar el empleado. Por favor, intente nuevamente.'));
        }
        //Se envían los datos a la vista
        $this->set(compact('employee'));
    }

    /**
     * Método eliminar
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //En caso de que se envíe la solicitud para eliminar un empleado mediante post o delete, ejecuta las instrucciones
        $this->request->allowMethod(['post', 'delete']);
        //Obtiene los datos del empleado mediante su id
        $employee = $this->Employees->get($id);
        //Valida si se pudo hacer la eliminación con éxito
        if ($this->Employees->delete($employee)) {
            //En caso de que se haya eliminado con éxito se envía un mensaje de éxito
            $this->Flash->success(__('El empleado se ha eliminado con éxito.'));
        } else {
            //En casod e que no se haya eliminado con éxito, envía un mensaje de error
            $this->Flash->error(__('No se puede eliminar el empleado. Por favor, intente nuevamente.'));
        }
        //Redirige al index del controlador de empleados
        return $this->redirect(['action' => 'index']);
    }

    //Función login
    public function login()
    {   
    	/*
		Se utilizó esta variable como auxiliar para evitar que se muestrw en el Layout de default las opciones de Cerrar sesión y menú principal del Layout default.ctp. Esta variable se crea si existe una sesión activa
    	*/
    	$usuario = $this->Auth->user();

    	//Valida si existe una sesión activa (si la variable $usuario tiene valores)
        if(!ISSET($usuario)){
        	//Esta variable se creará en el caso de que no exista ninguna sesión
        	$sesionNoActiva = 1;
        } 
        //Se envía la variable al Layout default.ctp
        $this->set(compact('sesionNoActiva'));

        //Valida que la llamada es mediante post
        if($this->request->is('post')) {
            //Ejecuta el método identify para verificar los datos
            $employee = $this->Auth->identify();
            //Valida si existen los datos en la BD de empleados
            if($employee){
                //En caso de que los datos sean correctos, crea la sesión con los datos del empleado
                $this->Auth->setUser($employee);
                //Redirecciona al Url establecido en la entidad
                return $this->redirect($this->Auth->redirectUrl());
            }
            //En caso de que los datos sean incorrectos, envía un mensaje de error
            $this->Flash->error(__('Nombre de usuario y/o contraseña inválido. Por favor, intente de nuevo.'));
        }

    }
    //Función para cerrar sesión
    public function logout(){
        //Ejecuta el método logout para cerrar sesión
        return $this->redirect($this->Auth->logout());

    }

    /**
    *Método para ver todos los registros de los empleados del departamento de Finance cuyo salario es mayor a 100,000
    *
    * @return \Cake\Http\Response|null
    */

    public function listaEmpleadosFinance(){

        //Construimos el query para buscar empleados del departamento de finanzas con sueldo mayor a 100,000
        $empleados = $this->Employees->find()
                //Seleccionamos los datos del empleado, del salario y del departamento que nos interese mostrar
        ->select(['Employees.emp_no', 'Employees.first_name', 'Employees.last_name', 's.salary', 'd.dept_no'])
                //Hacemos la asociación de empleado a salarios
        ->join([
                    //Especificamos con qué tabla se hará la asoaición
            'table' => 'Salaries',
                    //Especificamos qué alias usaremos para nombrar a la tabla
            'alias' => 's',
                    //Típo de join
            'type' => 'INNER',
                    //Escribimos las condiciones
            'conditions' => [
                's.salary >' => 100000, 
                's.emp_no = employees.emp_no'
            ]
        ])
        ->join([
                    //Especificamos con qué tabla se hará la asoaición
            'table' => 'dept_emp',
                    //Especificamos qué alias usaremos para nombrar a la tabla
            'alias' => 'd',
                    //Típo de join
            'type' => 'INNER',
                    //Escribimos las condiciones
            'conditions' => [
                'd.dept_no' => 'd002',
                'd.emp_no = employees.emp_no'
            ]
        ]);

        /*
        Tuve muchos problemas utilizando el siguiente código:

        $salarioEmpleado = $this->Salaries->find()
                 ->contain('Employees')
                 ->where(['salary >' => 100000]);

        Al no poder resolverlo utilzándolo, investigué y encontré una alternativa: la función join(), con la cual obtuve el resultado esperado.
        */


        //Se manda la información al componente para que sepa cómo mostrar los datos

        $empleadosFinance = $this->paginate($empleados);

        //Se manda la información ya paginada a la vista
        $this->set(compact('empleadosFinance'));

    }

    /*
	Función para el cambio de contraseña. Para resolver este problema, encontré el componente Auth con su función user(), en la documentación de CakePHP 3.8
    */
    public function cambiarContrasena(){

    	//Obtiene el número de empleado del usuario que tiene su sesión activa a través de la función user, del componente Auth.
        $id = $this->Auth->user('emp_no');
        
        //Obtiene los datos del empleado a través de su id
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);
        //En caso de que se envíe la solicitud para registrar un empleado mediante patch, post o put, ejecuta las instrucciones
        if ($this->request->is(['patch', 'post', 'put'])) {
            //Crea una nueva entidad para traer los datos del empleado que se quiere editar
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            //Se valida que se haya hecho el guardado con éxito
            if ($this->Employees->save($employee)) {
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Flash->success(__('Su contraseña se ha guardado con éxito'));
                //Se redirige al index del controlador de empleados
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que nos e guarden con éxito, se mandar un mensaje de error
            $this->Flash->error(__('No se puede cambiar la contraseña. Por favor, intente nuevamente'));
        }
        //Se envían los datos a la vista
        $this->set(compact('employee'));;
    }

}
