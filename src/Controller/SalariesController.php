<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Salaries Controller
 *
 * @property \App\Model\Table\SalariesTable $Salaries
 *
 * @method \App\Model\Entity\Salary[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalariesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $salaries = $this->paginate($this->Salaries);

        $this->set(compact('salaries'));
    }

    /**
     * View method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el from_date como parámetro pues es parte de la llave primaria
    public function view($id = null, $from_date = null)
    {
        // Se agregó el from_date como parámetro pues es parte de la llave primaria
        $salary = $this->Salaries->get([$id, $from_date]);
        // Se envían los datos del registro a la vista
        $this->set('salary', $salary);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   
        //Crea una entidad del tipo Salaries para agregar los nuevos datos
        $salary = $this->Salaries->newEntity();
        //En caso de que se envíe la solicitud para crear un registro, ejecuta las instrucciones
        if ($this->request->is('post')) {
            //Realiza la solicitud para guardar los datos
            $salary = $this->Salaries->patchEntity($salary, $this->request->getData());
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->Salaries->save($salary)) {
                //En caso de que se haya guardado los datos del registro con éxito, muestra un mensaje
                $this->Flash->success(__('El salario se ha guardado con éxito.'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo registro, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el salario. Por favor, intente nuevamente.'));
        }
        //Envía los datos del registro a la vista
        $this->set(compact('salary'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el from_date como parámetro pues es parte de la llave primaria
    public function edit($id = null, $from_date = null)
    {
        //Se obtiene la información de un registro a partir de sus llaves primarias
        $salary = $this->Salaries->get([$id, $from_date]);
        //Se solicita el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad para guardar los nuevos datos de la entidad a editar. Esto para poder editar la llave primaria*/
            $newSalarie = $this->Salaries->newEntity();
            //Elimina el registro que se editó para que no se duplique
            $this->Salaries->delete($salary);
            //Se obtiene la información del registro que queremos editar
            $newSalarie = $this->Salaries->patchEntity($newSalarie, $this->request->getData());
            //Valida si se guardó de forma correcta el registro
            if ($this->Salaries->save($newSalarie)) {
                //Se muestra un mensaje de que todo salió correctamente
                $this->Flash->success(__('El salario se ha guardado con éxito.'));
                //Se redirige a la p+agina del index del mismo controlador
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro, se muestra un mensaje de error
            $this->Flash->error(__('No se puede guardar el salario. Por favor, intente nuevamente.'));
        }
        //Se manda la información del título al editar a la vista
        $this->set(compact('salary'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Salary id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregó el from_date como parámetro pues es parte de la llave primaria
    public function delete($id = null, $from_date = null)
    {
        //Solicita el método utilizado para la eliminación
        $this->request->allowMethod(['post', 'delete']);
        //Obtiene los datos del registro mediante su id
        $salary = $this->Salaries->get([$id, $from_date]);
        //Valida si se pudo hacer la eliminación con éxito
        if ($this->Salaries->delete($salary)) {
            //En caso de que se haya eliminado con éxito se envía un mensaje de éxito
            $this->Flash->success(__('El salario se ha eliminado con éxito.'));
        } else {
            //En caso de que no se haya eliminado con éxito, envía un mensaje de error
            $this->Flash->error(__('No se puede eliminar el salario. Por favor, intente nuevamente.'));
        }
        //Redirige al index del controlador de DeptEmp
        return $this->redirect(['action' => 'index']);
    }
}
