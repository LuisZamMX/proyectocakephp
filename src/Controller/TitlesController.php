<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Titles Controller
 *
 * @property \App\Model\Table\TitlesTable $Titles
 *
 * @method \App\Model\Entity\Title[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TitlesController extends AppController
{
    /**
    *Método que inicializa el controlador
    *
    * @return \Cake\Http\Response|null
    */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Employees');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $titles = $this->paginate($this->Titles);

        $this->set(compact('titles'));
    }

    /**
     * View method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregaron title y from_date como parámetros porque forman parte de la llave primaria
    public function view($id = null, $title = null, $from_date = null)
    {
        // Se agregaron title y from_date como parámetros porque forman parte de la llave primaria
        $title = $this->Titles->get([$id, $title, $from_date]);
        // Envía los datos del título seleccionado a la vista
        $this->set('title', $title);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Crea una entidad del tipo Titles para agregar los nuevos datos
        $title = $this->Titles->newEntity();
        //En caso de que se envíe la solicitud para crear un registro, ejecuta las instrucciones
        if ($this->request->is('post')) {
            //Realiza la solicitud para guardar los datos
            $title = $this->Titles->patchEntity($title, $this->request->getData());
            //Valida que se haya realizado el guardado de la información con éxito
            if ($this->Titles->save($title)) {
                //En caso de que se haya guardado los datos del registro con éxito, muestra un mensaje
                $this->Flash->success(__('Título guardado con éxito.'));
                //Redirecciona hacia el index del controlador Employees
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya guardado con éxito la información del nuevo registro, muestra un mensaje de error.
            $this->Flash->error(__('No se puede guardar el título. Por favor, intente de nuevo'));
        }
        //Envía los datos del registro a la vista
        $this->set(compact('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // Se agregaron title y from_date como parámetros porque forman parte de la llave primaria
    public function edit($id = null, $title = null, $from_date = null)
    {
        //Se obtiene la información de un registro a partir de sus llaves primarias
        $title = $this->Titles->get([$id, $title, $from_date]);
        //Se solicita el tipo de petición
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*Se crea una nueva entidad para guardar los nuevos datos de la entidad a editar*/
            $newTitle = $this->Titles->newEntity();
            
            //A partir del registro que queremos editar, se sobrescribe la información que se haya cambiado
            $newTitle = $this->Titles->patchEntity($newTitle, $this->request->getData());
            //Se pregunta si se guardó de forma correcta el registro
            if ($this->Titles->save($newTitle)) {
                //En caso de que se haya guardado con éxito, se envia un mensaje de éxito.
                $this->Titles->delete($title);
                //Se muestra un mensaje de que todo salió correctamente
                
                $this->Flash->success(__('Título guardado con éxito.'));
                //Se redirige a la p+agina del index del mismo controlador 
                return $this->redirect(['action' => 'index']);
            }
            //En caso de que no se haya podido guardar el registro, se muestra un mensaje de error
            $this->Flash->error(__('No se puede guardar el título. Por favor, intente de nuevo'));
        }
        //Se manda la información del título al editar a la vista
        $this->set(compact('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Title id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    // Se agregaron title y from_date como parámetros porque forman parte de la llave primaria
    public function delete($id = null, $title = null, $from_date = null)
    {
        //Se restringen qué tipos de peticiones pueden llamar a este método
        $this->request->allowMethod(['post', 'delete']);
        //Se obtiene la información de un registro dependiendo de sus llaves primarias.
        $title = $this->Titles->get([$id, $title, $from_date]);
        //Se pregunta si se eliminó de forma correcta el registro
        if ($this->Titles->delete($title)) {
            //Se muestra un mensaje de que todo salió correctamente
            $this->Flash->success(__('Título eliminado con éxito.'));
        } else {
            //En caso de que no se haya podido guardar el registro se muestra un mensaje de erros
            $this->Flash->error(__('No se puede eliminar el título. Por favor intente de nuevo'));
        }
        //Se redirige la página al index de ese mismo controlador
        return $this->redirect(['action' => 'index']);
    }

    /**
    *Método para ver todos los registros de los títulos en donde el empleado del título es mujer
    *
    * @return \Cake\Http\Response|null
    *
    */
    public function listaMujeres()
    {
        //Se buscan todos los títulos que sean de empleados mujeres
        $titles = $this->Titles->find()
            ->contain('Employees')
            ->where(['Employees.gender' => 'F']);
        //Se manda la información al componente para que sepa cómo mostrar los datos
        $titulosMujeres = $this->paginate($titles);
        //Se manda la información ya paginada a la vista
        $this->set(compact('titulosMujeres'));
    }
}
