<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Employees Model
 *
 * @method \App\Model\Entity\Employee get($primaryKey, $options = [])
 * @method \App\Model\Entity\Employee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Employee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Employee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Employee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Employee findOrCreate($search, callable $callback = null, $options = [])
 */
class EmployeesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        //Especifica el nombre de la tabla en la base de datos
        $this->setTable('employees');
        $this->setDisplayField('emp_no');
        //Especifica la llave primaria de la tabla
        $this->setPrimaryKey('emp_no');

        /*
        Asociaciones
        hasMany: Uno a muchos
        belongsTo: Muchos a uno y uno a uno
        belongToMany: Muchos a muchos
        setForeignKey: Llave foránea
        */
        $this   ->hasMany('titles')
                ->setForeignKey('emp_no');

        $this   ->hasMany('deptEmp')
                ->setForeignKey('emp_no');

        $this   ->hasMany('DeptManager')
                ->setForeignKey('emp_no');

        $this   ->hasMany('Salaries')
                ->setForeignKey('emp_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        /*
        VALIDACIONES FORMULARIO
        integer: Entero
        date: Fecha
        scalar: Escalar
        allowEmptyString: Acepta cadena vacía
        requirePresence: Necesita estar presente en el arreglo de datos
        notEmptyDate: No acepta fechas vacías
        maxLength: Establece la longitud máxima de la cadena
        inList: Verifica que el valor ingresado coincida con un arreglo de valores establecido
        notEmptyString: No acepta cadenas vacías
        */
        $validator
            ->integer('emp_no')
            ->allowEmptyString('emp_no', null, 'create');

        $validator
            ->date('birth_date')
            ->requirePresence('birth_date', 'create')
            ->notEmptyDate('birth_date');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 14)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 16)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('gender')
            ->requirePresence('gender', 'create')
            ->notEmptyString('gender')
            ->inList('gender', array("F", "M"), 'Ingrese un dato válido (F | M)');

        $validator
            ->date('hire_date')
            ->requirePresence('hire_date', 'create')
            ->notEmptyDate('hire_date');

        $validator
            ->scalar('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        return $validator;
    }
    
    public function buildRules(RulesChecker $rules)
    {   
        //Agrega una regla
        $rules->add(
            //Verifica que no exista una cuenta vinculada a esa dirección de correo electrónico.
            $rules->isUnique(
                ['email'],  
                'Ya existe una cuenta con ese correo electrónico.'
            )
        );

        return $rules;
    }

    //Función para obtener el ID del último empleado registrado, pues el campo de emp_no, no es auto incremental
    public function nextID()
    {
        return $this->find()->select(['MaxEmp'=>'MAX(Employees.emp_no) + 1'])->first();
    }

}
