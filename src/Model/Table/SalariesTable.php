<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Salaries Model
 *
 * @method \App\Model\Entity\Salary get($primaryKey, $options = [])
 * @method \App\Model\Entity\Salary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Salary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Salary|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salary saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Salary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Salary findOrCreate($search, callable $callback = null, $options = [])
 */
class SalariesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        //Especifica el nombre de la tabla en la base de datos
        $this->setTable('salaries');
        $this->setDisplayField('emp_no');
        //La llave primaria es compuesta
        $this->setPrimaryKey(['emp_no', 'from_date']);

        //Asociaciones
        $this   ->belongsTo('Employees')
        ->setForeignKey('emp_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        /*
        VALIDACIONES FORMULARIO
        integer: Entero
        date: Fecha
        scalar: Escalar
        allowEmptyString: Acepta cadena vacía
        requirePresence: Necesita estar presente en el arreglo de datos
        notEmptyDate: No acepta fechas vacías
        maxLength: Establece la longitud máxima de la cadena
        inList: Verifica que el valor ingresado coincida con un arreglo de valores establecido
        notEmptyString: No acepta cadenas vacías
        */
        
        $validator
            ->integer('emp_no')
            ->requirePresence('emp_no', 'create')
            ->notEmpty('emp_no');

        $validator
            ->integer('salary')
            ->requirePresence('salary', 'create')
            ->notEmptyString('salary');

        $validator
            ->date('from_date')
            ->notEmptyDate('from_date');

        $validator
            ->date('to_date')
            ->requirePresence('to_date', 'create')
            ->notEmptyDate('to_date');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {   
        //Agrega una regla
        $rules->add(
            //Verifica que exista el número ingresado como llave primaria de un empleado. De lo contrario, notifica que no se ha encontrado la llave foránea.
            $rules->existsIn(
                ['emp_no'], 
                'employees', 
                'Llave foránea no encontrada.'
            )
        );

        return $rules;
    }
}
