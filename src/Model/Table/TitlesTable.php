<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Titles Model
 *
 * @method \App\Model\Entity\Title get($primaryKey, $options = [])
 * @method \App\Model\Entity\Title newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Title[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Title|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Title saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Title patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Title[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Title findOrCreate($search, callable $callback = null, $options = [])
 */
class TitlesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('titles');
        $this->setDisplayField('title');
        $this->setPrimaryKey(['emp_no', 'title', 'from_date']);

        //Asociaciones
        $this   ->belongsTo('Employees')
        ->setForeignKey('emp_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        /*
        VALIDACIONES FORMULARIO
        integer: Entero
        date: Fecha
        scalar: Escalar
        allowEmptyString: Acepta cadena vacía
        requirePresence: Necesita estar presente en el arreglo de datos
        notEmptyDate: No acepta fechas vacías
        maxLength: Establece la longitud máxima de la cadena
        inList: Verifica que el valor ingresado coincida con un arreglo de valores establecido
        notEmptyString: No acepta cadenas vacías
        */
        
        $validator
        ->integer('emp_no')
        ->requirePresence('emp_no')
        ->notEmpty('emp_no');

        $validator
        ->scalar('title')
        ->requirePresence('title')
        ->notEmpty('emp_no')
        ->maxLength('title', 50);

        $validator
        ->date('from_date')
        ->requirePresence('from_date')
        ->notEmpty('from_date');

        $validator
        ->date('to_date')
        ->allowEmptyDate('to_date');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {   
        //Agrega una regla
        $rules->add(
            //Verifica que exista el número ingresado como llave primaria de un empleado. De lo contrario, notifica que no se ha encontrado la llave foránea.
            $rules->existsIn(
                ['emp_no'], 
                'employees', 
                'Llave foránea no encontrada.'
            )
        );

        return $rules;
    }

}




