<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department[]|\Cake\Collection\CollectionInterface $departments
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo departamento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="departments index large-9 medium-8 columns content">
    <h3><?= __('Departments') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('dept_no', 'No. Departamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dept_name', 'Nombre') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($departments as $department): ?>
            <tr>
                <td><?= h($department->dept_no) ?></td>
                <td><?= h($department->dept_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $department->dept_no]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $department->dept_no]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $department->dept_no], ['confirm' => __('¿Está seguro que desea eliminar el departamento # {0}?', $department->dept_no)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginador');?> 

</div>
