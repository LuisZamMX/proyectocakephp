<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Department $department
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar departamento'), ['action' => 'edit', $department->dept_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar departamento'), ['action' => 'delete', $department->dept_no], ['confirm' => __('¿Está seguro que desea eliminar el departamento # {0}?', $department->dept_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de departamentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo departamento'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="departments view large-9 medium-8 columns content">
    <h3>Departamento <?= h($department->dept_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Departamento') ?></th>
            <td><?= h($department->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($department->dept_name) ?></td>
        </tr>
    </table>
</div>
