<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp $deptEmp
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $deptEmp->emp_no, $deptEmp->dept_no],
                ['confirm' => __('¿Está seguro que desea eliminar el registro del empleado # {0}?', $deptEmp->emp_no)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista de empleados por departamento'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="deptEmp form large-9 medium-8 columns content">
    <?= $this->Form->create($deptEmp) ?>
    <fieldset>
        <legend><?= __('Editar registro') ?></legend>
        <?php
            echo $this->Form->control('emp_no', ['label' => 'No. Empleado', 'type' => 'number']);
            echo $this->Form->control('dept_no', ['label' => 'No. Departamento', 'type' => 'text']);
            echo $this->Form->control('from_date', [
                'label' => 'Fecha inicio',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date('Y') + 50,
                'monthNames' => false
            ]);
            echo $this->Form->control('to_date', [
                'label' => 'Fecha fin',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'monthNames' => false
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
