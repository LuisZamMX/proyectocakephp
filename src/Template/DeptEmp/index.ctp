<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp[]|\Cake\Collection\CollectionInterface $deptEmp
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo registro'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deptEmp index large-9 medium-8 columns content">
    <h3><?= __('Empleados por departamento') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dept_no', 'No. Departamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($deptEmp as $deptEmp): ?>
            <tr>
                <td><?= $this->Number->format($deptEmp->emp_no) ?></td>
                <td><?= h($deptEmp->dept_no) ?></td>
                <td><?= h($deptEmp->from_date->format('Y-m-d')) ?></td>
                <td><?= h($deptEmp->to_date->format('Y-m-d')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $deptEmp->emp_no, $deptEmp->dept_no]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $deptEmp->emp_no, $deptEmp->dept_no]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $deptEmp->emp_no, $deptEmp->dept_no], ['confirm' => __('¿Está seguro que desea eliminar el registro del empleado # {0}?', $deptEmp->emp_no)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginador');?> 

</div>
