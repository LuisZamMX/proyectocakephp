<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptEmp $deptEmp
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar registro'), ['action' => 'edit', $deptEmp->emp_no, $deptEmp->dept_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar registro'), ['action' => 'delete', $deptEmp->emp_no, $deptEmp->dept_no], ['confirm' => __('¿Está seguro que desea eliminar el registro del empleado # {0}?', $deptEmp->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de empleados por departamento'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo registro'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deptEmp view large-9 medium-8 columns content">
    <h3>Empleado #<?= h($deptEmp->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Departamento') ?></th>
            <td><?= h($deptEmp->dept_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($deptEmp->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha inicio') ?></th>
            <td><?= h($deptEmp->from_date->format('Y-m-d')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha fin') ?></th>
            <td><?= h($deptEmp->to_date->format('Y-m-d')) ?></td>
        </tr>
    </table>
</div>
