<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeptManager $deptManager
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista de administradores por departamento'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="deptManager form large-9 medium-8 columns content">
    <?= $this->Form->create($deptManager) ?>
    <fieldset>
        <legend><?= __('Agregar administrador por departamento') ?></legend>
        <?php
            echo $this->Form->control('emp_no', ['label' => 'No. Empleado', 'type' => 'number']);
            echo $this->Form->control('dept_no', ['label' => 'No. Departamento', 'type' => 'text']);
            echo $this->Form->control('from_date', [
                'label' => 'Fecha inicio',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date('Y') + 50,
                'monthNames' => false
            ]);
            echo $this->Form->control('to_date', [
                'label' => 'Fecha fin',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 50,
                'maxYear' => date('Y') + 50,
                'monthNames' => false
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
