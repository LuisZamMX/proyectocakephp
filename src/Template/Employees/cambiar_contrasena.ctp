<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        
        <li><?= $this->Html->link(__('Cancelar'), ['controller' => 'pages', 'action' => 'home']) ?></li>
    </ul>
</nav>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('Cambiar contraseña') ?></legend>
        <p>Usuario: <?= h($employee->email); ?></p>
        <?php
            echo $this->Form->control('password', [
                'label' => 'Nueva contraseña', 
                'type' => 'password', 
                'value' => ''
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
