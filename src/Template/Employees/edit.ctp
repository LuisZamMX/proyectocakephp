<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $employee->emp_no],
                ['confirm' => __('Estás seguro que deseas eliminar el empleado # {0}?', $employee->emp_no)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista de empleados'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="employees form large-9 medium-8 columns content">
    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('Editar empleado') ?></legend>
        <?php
            echo $this->Form->control('birth_date', [
                'label' => 'Fecha de nacimiento',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 100,
                'maxYear' => date('Y') - 18,
                'monthNames' => false
            ]);
            echo $this->Form->control('first_name', ['label' => 'Nombre(s)', 'type' => 'text']);
            echo $this->Form->control('last_name', ['label' => 'Apellidos', 'type' => 'text']);
            echo $this->Form->control('gender', ['label' => 'Género', 'type' => 'text']);
            echo $this->Form->control('hire_date', [
                'label' => 'Fecha de contratación',
                'type' => 'date',
                'empty' => true,
                'minYear' => date('Y') - 100,
                'maxYear' => date('Y'),
                'monthNames' => false
            ]);
            echo $this->Form->control('email', ['label' => 'Correo electrónico', 'type' => 'email']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
