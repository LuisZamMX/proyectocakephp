<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee[]|\Cake\Collection\CollectionInterface $employees
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Regresar a Empleados'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="employees index large-9 medium-8 columns content">
    <h3><?= __('Empleados') ?></h3>
    <p>Empleados que pertenecen al departamento Finance y que tienen un sueldo mayor a 100,000 </p>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name', 'Nombres') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name', 'Apellidos') ?></th>
                <th scope="col" class="actions"><?= __('Salario') ?></th>
                <th scope="col" class="actions"><?= __('Departamento') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empleadosFinance as $employee): ?>
            <tr>
                <td><?= $this->Html->link($this->Number->format($employee->emp_no), ['controller'=> 'employees', 'action' => 'view', $employee->emp_no]) ?></td>
                <td><?= h($employee->first_name) ?></td>
                <td><?= h($employee->last_name) ?></td>
                <td class="actions">
                    <?= h($this->Number->format($employee->s['salary'])) ?>
                </td>
                <td><?= h($employee->d['dept_no']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginador');?> 

</div>
