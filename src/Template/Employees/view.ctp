<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar empleado'), ['action' => 'edit', $employee->emp_no]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar empleado'), ['action' => 'delete', $employee->emp_no], ['confirm' => __('Are you sure you want to delete # {0}?', $employee->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de empleados'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo empleado'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="employees view large-9 medium-8 columns content">
    <h3>Empleado #<?= h($employee->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($employee->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellidos') ?></th>
            <td><?= h($employee->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Género') ?></th>
            <td><?= h($employee->gender) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($employee->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de nacimiento') ?></th>
            <td><?= h($employee->birth_date->format('Y-d-m')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de contratación') ?></th>
            <td><?= h($employee->hire_date->format('Y-d-m')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($employee->email) ?></td>
        </tr>
    </table>
</div>
