<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Proyecto: CakePHP';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <!-- Esta función trae la variable sesionNoActiva de la función login de Employees Controller para verificar si existe una cuenta y en caso de no existir, no muestra el título de la tabla, sino muestra Inicio de sesión -->
            <?php 
                if(!ISSET($sesionNoActiva)){
            ?>
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
            <?php
                }else{
            ?>
            <li class="name">
                <h1><a href="">Proyecto CakePHP</a></h1>
            </li>
            <?php
                }
            ?>
        </ul>
        <div class="top-bar-section">
            <!-- Esta función trae la variable sesionNoActiva de la función login de Employees Controller para verificar si existe una cuenta y en caso de no existir, no muestra las opciones de Cerrar Sesión y Menú principal -->
            <?php 
                if(!ISSET($sesionNoActiva)){
            ?>
            <ul class="right">
                <li><?= $this->Html->link(__('Menú principal'), ['controller' => 'pages', 'action' => 'home']) ?></li>
                <li><?= $this->Html->link(__('Cerrar sesión'), ['controller' => 'employees', 'action' => 'logout']) ?></li>
            </ul>
            <?php
                }
            ?>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
