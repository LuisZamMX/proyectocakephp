<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

// Se cambia la variable para que en el navegador aparezca mipropia descripción
$cakeDescription = 'Proyecto: Cake PHP';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <!-- Agregué los archivos para bootstrap en webroot y los llamo con la función Html->css y Html->script -->
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->script('jquery.js') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body>
    
    <!-- Header con estilos de bootstrap -->
    <header class="bg-danger text-white home-header p-5">
        <div class="container">
            <div class="d-flex flex-column ">
                <h1 class="text-center mb-4">Mi primer proyecto con</h1>
                <!-- La función Html->image carga imágenes que se encuentren en webroot/img -->
                <?= $this->Html->image('cake.logo.svg') ?></div>
            </div>
        </div>
    </header>

    <main>
        <!-- Estilos con bootstrap -->
        <div class="container mt-5">
            <div class="jumbotron">
                <h1 class="display-4">¡Bienvenido!</h1>
                <p class="lead">Este es mi proyecto Final del curso Cake PHP 3.8. </p>
                <hr class="my-4">
                <p>Para continuar elija alguna de las siguientes opciones:</p>
            </div>
            
            <!-- Elemento nav de Bootstrap 4 -->
            <ul class="nav flex-column  border ">
                <li class="nav-item p-2 pl-3">
                    <!-- Mediante esta función se crean los links para el menú -->
                    <a href=""><?= $this->Html->link(__('Empleados'), ['controller' => 'employees', 'action' => 'index']) ?></a>
                </li>
                <li class="nav-item p-2 pl-3 bg-light">
                    <?= $this->Html->link(__('Títulos'), ['controller' => 'titles', 'action' => 'index']) ?>
                </li>
                <li class="nav-item p-2 pl-3">
                    <?= $this->Html->link(__('Salarios'), ['controller' => 'salaries', 'action' => 'index']) ?>
                </li>
                <li class="nav-item p-2 pl-3 bg-light">
                    <?= $this->Html->link(__('Departamentos'), ['controller' => 'departments', 'action' => 'index']) ?>
                </li>
                <li class="nav-item p-2 pl-3">
                    <?= $this->Html->link(__('Empleados por departamento'), ['controller' => 'dept_emp', 'action' => 'index']) ?>
                </li>
                <li class="nav-item p-2 pl-3 bg-light">
                    <?= $this->Html->link(__('Administradores por departamento'), ['controller' => 'dept_manager', 'action' => 'index']) ?>
                </li>
                <li class="nav-item p-2 pl-3">
                    <?= $this->Html->link(__('Cambiar contraseña'), ['controller' => 'employees', 'action' => 'cambiarContrasena']) ?>
                </li>
                <li class="nav-item p-2 pl-3 bg-light">
                    <?= $this->Html->link(__('Cerrar sesión'), ['controller' => 'employees', 'action' => 'logout']) ?>
                </li>
            </ul>



    </div>
</main>



</body>
</html>
