<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary[]|\Cake\Collection\CollectionInterface $salaries
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo salario'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salaries index large-9 medium-8 columns content">
    <h3><?= __('Salarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('salary', 'Salario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salaries as $salary): ?>
            <tr>
                <td><?= $this->Number->format($salary->emp_no) ?></td>
                <td><?= $this->Number->format($salary->salary) ?></td>
                <td><?= h($salary->from_date->format('Y-m-d')) ?></td>
                <td><?= h($salary->to_date->format('Y-m-d')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $salary->emp_no, $salary->from_date->format('Y-m-d')]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $salary->emp_no, $salary->from_date->format('Y-m-d')]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $salary->emp_no, $salary->from_date->format('Y-m-d')], ['confirm' => __('¿Está seguro que desea eliminar el salario del empleado # {0}?', $salary->emp_no)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php echo $this->element('paginador');?>

    </div>
</div>
