<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salary $salary
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar salario'), ['action' => 'edit', $salary->emp_no, $salary->from_date->format('Y-m-d')]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar salario'), ['action' => 'delete', $salary->emp_no, $salary->from_date->format('Y-m-d')], ['confirm' => __('¿Está seguro que desea eliminar el salario del empleado # {0}?', $salary->emp_no)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de salarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo salario'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salaries view large-9 medium-8 columns content">
    <h3>Empleado #<?= h($salary->emp_no) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($salary->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Salario') ?></th>
            <td><?= $this->Number->format($salary->salary) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha inicio') ?></th>
            <td><?= h($salary->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha fin') ?></th>
            <td><?= h($salary->to_date) ?></td>
        </tr>
    </table>
</div>
